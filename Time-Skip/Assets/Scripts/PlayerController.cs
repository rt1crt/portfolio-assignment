﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


	[RequireComponent(typeof(CapsuleCollider))]							//These are used to ensure that the required components are avliable reducing the chances of errors
	[RequireComponent(typeof(Rigidbody))]

public class PlayerController : MonoBehaviour {

	public float forwardSpeed = 7f;
	public float backwardsSpeed = 2f;
	public float turningSpeed = 120f;
	private CapsuleCollider col;
	private Rigidbody rb;
	private Vector3 velocity;
	private Animator ani;
    public Camera cam;
	public Image DeathImage;
	public Color DeathColour;
	public AudioClip step;
    public AudioSource AS;
	public int keysCollected = 0;


    void Start () {
		
		col = GetComponent<CapsuleCollider>();
		rb = GetComponent<Rigidbody>();
		ani = GetComponent<Animator>();
		cam = Camera.main;
		
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate()
	{
		rb.useGravity = true;
		PlayerMovement();
	}

	void PlayerMovement()
	{
		float vertical = Input.GetAxis("Vertical");
		float horizontal = Input.GetAxis("Horizontal");
		float mouseX = Input.GetAxis("Mouse X");

      

		velocity = new Vector3 (horizontal,0f, vertical);
		velocity.Normalize();
		velocity = transform.TransformDirection (velocity);
		ani.SetFloat("Speed", vertical);
		ani.SetFloat("Direction", mouseX);
		if(vertical >= 0.1)
		{
			velocity *= forwardSpeed;
		}
		else if (vertical <= 0.1)
		{
			velocity *= backwardsSpeed;
		}
		

		transform.localPosition += velocity * Time.fixedDeltaTime;

        //	transform.Rotate(0,(horizontal * turningSpeed), 0);
        transform.rotation = Quaternion.Euler(0, cam.transform.eulerAngles.y, 0);
    }

	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.tag == "Enemy")
		{
            DeathImage.color = DeathColour;
			Invoke("Dead", 2);

		}
	}

	void Dead()
	{
			Scene scene = SceneManager.GetActiveScene();
			SceneManager.LoadScene(scene.name);
			
	}

	void Step()
    {
        AS.PlayOneShot(step);
    }
}
