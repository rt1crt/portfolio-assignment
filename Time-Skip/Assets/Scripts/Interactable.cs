﻿using UnityEngine;

public class Interactable : MonoBehaviour {

	public float radius = 3f;
	bool isFocus = false;
	bool hasInteracted = false;
	Transform player;
	public virtual void Interact()															//This is pure virtual so that I can use this script as the base for many different intereactables.
	{

	}

	void Update()
	{
		if (isFocus && !hasInteracted)
		{
			float distance = Vector3.Distance(player.position, transform.position);
			if(distance <= radius)
			{
				Interact();
				Debug.Log("interact");
				hasInteracted = true;
			}
		}
	}
	public void OnFocused (Transform playerTransform)
	{
		isFocus = true;
		player = playerTransform;
		hasInteracted = false;
	}

	public void notFocused()
	{
		isFocus = false;
		player = null;
		hasInteracted = false; 
	}
	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere(transform.position, radius);
	}


}
