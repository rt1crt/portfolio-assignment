﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject[] enemy;
    public Transform[] spawnPoint;

    public float spwanTime = 20f;


    void Start()
    {
        InvokeRepeating ("EnemySpawn", spwanTime, spwanTime);
    }

    void EnemySpawn()
    {
        int spawnPointIndx = Random.Range (0, spawnPoint.Length);
        int enemyIndx = Random.Range(0, enemy.Length);

        Instantiate (enemy[enemyIndx], spawnPoint[spawnPointIndx].position, spawnPoint[spawnPointIndx].rotation);
    }
}
