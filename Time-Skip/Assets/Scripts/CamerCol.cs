﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamerCol : MonoBehaviour {
	
	public float minDis = 1f;
	public float maxDis = 4f;
	public float smooth = 10f;
	Vector3 dollyDirec;
	public Vector3 dollyDirecAdj;
	public float distance;


	void Awake () {
		dollyDirec = transform.localPosition.normalized;
		distance = transform.localPosition.magnitude;
	}
	
	// Update is called once per frame
	void Update () {
		
		Vector3 desCamPos = transform.parent.TransformPoint(dollyDirec * maxDis);

		RaycastHit hit;

		if(Physics.Linecast(transform.parent.position, desCamPos, out hit))
		{
			distance = Mathf.Clamp((hit.distance * 0.6f), minDis, maxDis);
		}
		else
		{
			{
				distance = maxDis;
			}
		}
		transform.localPosition = Vector3.Lerp(transform.localPosition, dollyDirec * distance, Time.deltaTime * smooth);
	}
}
