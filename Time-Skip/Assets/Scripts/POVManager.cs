﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class POVManager : MonoBehaviour {

	public Camera thirdPerson;
	public Camera firstPerson;

	public bool firstPersonActive = false;
	
	
	void Start()
	{
		thirdPerson.enabled = true;
		firstPerson.enabled = false;
	}

	void Update () {
		
		if(Input.GetKeyDown(KeyCode.R) == true && firstPersonActive == false)
		{
			firstPersonActive = true;
			thirdPerson.enabled = false;
			firstPerson.enabled = true;
		}
		else if(Input.GetKeyDown(KeyCode.R) == true && firstPersonActive == true)
		{
			firstPersonActive = false;
			thirdPerson.enabled = true;
			firstPerson.enabled = false;
		}

	}
}
