﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeSkip : MonoBehaviour {

	private int present = 11;
	private int future = 12;
	public GameObject player;
    public Image TimeSkipImage;
    public AudioClip TimeSound;
    public float flashSpeed = 5f;
    public Color TimeColour;
	bool inFuture = false;
    private bool timeSkipping;
    public Camera firstPerson;
    public Camera thirdPerson;



	void Update()
	{
		if(Input.GetMouseButtonDown(0) && inFuture != true)
		{
            //player.layer = future;
            ChangeLayers(player, future);
			inFuture = true;
            timeSkipping = true;
            CullingMaskChange();
        }
		else if(Input.GetMouseButtonDown(0)) 
		{
			//player.layer = present;
            ChangeLayers(player, present);
            inFuture = false;
            timeSkipping = true;
            CullingMaskChange();

        }
        ScreenFlash();
	}

    void CullingMaskChange()
    {
        if(player.layer == future)
        {
            firstPerson.cullingMask = 1 << 0 | 0 << present | 1 << future;
            thirdPerson.cullingMask = 1 << 0 | 0 << present | 1 << future;
            
        }
        else if(player.layer == present)
        {
            firstPerson.cullingMask = 1 << 0 | 1 << present | 0 << future;
            thirdPerson.cullingMask = 1 << 0 | 1 << present | 0 << future;
        }
    }

    public static void ChangeLayers(GameObject player, int layer)                   //This will go through all of the children in the object to ensure that they are all changed to the correct layer.
    {
        player.layer = layer;
        foreach (Transform child in player.transform)
        {
            ChangeLayers(child.gameObject, layer);
        }
    }

    void ScreenFlash()
    {
        if (timeSkipping == true)
        {
            TimeSkipImage.color = TimeColour;
        }
        else
        {
            TimeSkipImage.color = Color.Lerp(TimeSkipImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }

        timeSkipping = false;
        
    }
}
