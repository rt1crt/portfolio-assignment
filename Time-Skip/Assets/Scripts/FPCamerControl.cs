﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPCamerControl : MonoBehaviour {

	Vector2 mouseLook;
	Vector2 smoothV;
	public float minimumVert = -45f;
	public float maximumVert = 45f;
	public float sensi = 5f;
	public float smoothing = 2f;
	public GameObject charac;
	public Camera cam;
	GameObject manager;
	POVManager povManager;

	void Start()
	{
		manager = GameObject.Find("POVManager");
		povManager = manager.GetComponent<POVManager>();
	}
	

	void Update()
	{
		
		if(povManager.firstPersonActive == true)
		{
		/*	var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

			md = Vector2.Scale(md, new Vector2(sensi * smoothing, sensi * smoothing));
			smoothV.x = Mathf.Lerp(smoothV.x, md.x, 1f / smoothing);
			smoothV.y = Mathf.Lerp(smoothV.y, md.y, 1f / smoothing);
			mouseLook += smoothV;
			mouseLook.y = Mathf.Clamp (mouseLook.y, minimumVert, maximumVert);
			transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
			charac.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, charac.transform.up);
		*/
		transform.rotation = Quaternion.Euler(cam.transform.eulerAngles.x, cam.transform.eulerAngles.y, 0);
		}
		else if(povManager.firstPersonActive == false)
		{
			transform.rotation = Quaternion.Euler(cam.transform.eulerAngles.x, cam.transform.eulerAngles.y, 0);
		}

	}


}
