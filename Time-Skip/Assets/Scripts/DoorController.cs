﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour {

	public Animator ani;
	public BoxCollider trigger;
	public bool locked = false;
	

	void Start () {
		ani = GetComponent<Animator>();
		trigger = GetComponent<BoxCollider>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider col)
	{
		if(locked == true)
		{
			ani.SetBool("isLocked", true);
		}
		else if(locked == false)
		{
			ani.SetBool("isLocked", false);
		}
		ani.SetBool("character_nearby", true);
	}

	void OnTriggerExit(Collider col)
	{
		ani.SetBool("character_nearby", false);
	}
}
