﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableSpawner : MonoBehaviour
{
    public GameObject[] collectable;
    public Transform[] spawnPoint;
    void Start()
    {
        CollectableSpawn();
    }

    public void CollectableSpawn()
    {
        int spawnPointIndx = Random.Range (0, spawnPoint.Length);
        int collectableIndx = Random.Range(0, collectable.Length);

        Instantiate (collectable[collectableIndx], spawnPoint[spawnPointIndx].position, spawnPoint[spawnPointIndx].rotation);
    }
}
