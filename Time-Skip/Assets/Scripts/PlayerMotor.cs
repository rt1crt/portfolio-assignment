﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class PlayerMotor : MonoBehaviour {

	NavMeshAgent agent;
	public float lookSpeed = 5f;
	Transform target;
	// Use this for initialization
	void Start () {
		agent = GetComponent<NavMeshAgent>();
	}
	
	void Update()
	{
		if(target != null)
		{
			agent.SetDestination(target.position);
			FaceTarget();
		}
	}
	public void MoveToPoint (Vector3 point)
	{
		agent.SetDestination(point);
	}

	public void FollowTarget(Interactable newTarget)
	{
		agent.stoppingDistance = newTarget.radius * 0.8f;
		agent.updateRotation = false;
		target = newTarget.transform;
	}

	public void StopFollow()
	{
		target = null;
		agent.stoppingDistance = 0f;
		agent.updateRotation = true;
	}
	void FaceTarget()
	{
		Vector3 direction = (target.position - transform.position).normalized;										//This works out the direction towards our target
		Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0f, direction.z));				//This will then check to see how they shoudl rotate towards the target, a vector 3 is used to ensure the model isn't rotated on the y axis.
		transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * lookSpeed);		//This will then smoothly turn towards that location.
	}
}
