﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour {

	public float moveSpeed = 120f;
	public GameObject CameraFollowTarget;
	Vector3 FollowPos;
	public float angleClamp = 80f;
	public float sensitivity =150f;
	public GameObject Camera;
	public GameObject Player;


	private float rotationY = 0f;
	private float rotationX = 0f;

	void Start () {
		Vector3 rotation = transform.localRotation.eulerAngles;
		rotationY = rotation.y;
		rotationX = rotation.x;
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		
	}
	
	// Update is called once per frame
	void Update () {
		
		float InputX = Input.GetAxis("Mouse X");
		float InputZ = Input.GetAxis("Mouse Y");
		 
		 rotationY += InputX * sensitivity *Time.deltaTime;
		 rotationX -= InputZ * sensitivity *Time.deltaTime;

		 rotationX = Mathf.Clamp(rotationX, -angleClamp, angleClamp);

		 Quaternion localRotation = Quaternion.Euler(rotationX, rotationY, 0f);
		 transform.rotation = localRotation;

		 

	}

	void LateUpdate()
	{
		CameraUpdate();

    }

	void CameraUpdate()
	{
		Transform target = CameraFollowTarget.transform;

		float step = moveSpeed * Time.deltaTime;
		transform.position = Vector3.MoveTowards(transform.position, target.position, step);
	}
}
