﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour
{
    private CapsuleCollider capCol;
    private GameObject Manager;
    private GameObject Player;
    // Start is called before the first frame update
    void Start()
    {
        capCol = GetComponent<CapsuleCollider>();
        Manager = GameObject.Find("CollectablesManager");
        Player = GameObject.Find("PlayerCharacterAni");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag == "Player")
		{
            Player.GetComponent<PlayerController>().keysCollected += 1;
            Manager.GetComponent<CollectableSpawner>().CollectableSpawn();
            Destroy(gameObject);
		}
    }

    
}
