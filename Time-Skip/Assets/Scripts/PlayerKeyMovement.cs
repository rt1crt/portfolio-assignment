﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerKeyMovement : MonoBehaviour {

	public float MoveSpeed = 3;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		playerMovement();
	}
	void playerMovement()
	{
		float horizontal = Input.GetAxis("Horizontal");
		float vertical = Input.GetAxis("Vertical");
		Vector3 playerMovement = new Vector3(horizontal, 0f, vertical) * MoveSpeed * Time.deltaTime;
		transform.Translate(playerMovement, Space.Self);
	}
}
