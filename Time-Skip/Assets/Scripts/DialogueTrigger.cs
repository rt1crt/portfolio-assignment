﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour {

	public Dialogue dialogue;

	public BoxCollider col;

	public void Trigger()
	{
		FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
	}

	void OnTriggerEnter(Collider col)
	{
		FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
	}
	
	void OnTriggerStay(Collider col)
	{
		if (Input.GetKeyDown(KeyCode.Mouse1))
		{
			FindObjectOfType<DialogueManager>().DisplayNextLine();
		}
	}

	void OnTriggerExit(Collider col)
	{
		FindObjectOfType<DialogueManager>().EndConvo();
	}

}
 