﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour {

	private Queue<string> words;
	
	public Text charName;
	public Text dialogueText;
	public GameObject dialogueBox;

	void Start () {

		words = new Queue<string>();

	}

	public void StartDialogue(Dialogue dialogue)
	{

		charName.text = dialogue.name;
		dialogueBox.SetActive(true);
		words.Clear();

		foreach(string word in dialogue.words)
		{
			words.Enqueue(word);
		}

		DisplayNextLine();

	}

	public void DisplayNextLine()
	{
		if(words.Count == 0)
		{
			EndConvo();
			return;
		}

		string word = words.Dequeue();
		StopAllCoroutines();
		StartCoroutine(TypeWords(word));

	}

	IEnumerator TypeWords (string word)
	{
		dialogueText.text = "";
		foreach(char letter in word.ToCharArray())
		{
			dialogueText.text += letter;
			yield return null;
		}
	}

	public void EndConvo()
	{
		dialogueBox.SetActive(false);
	}
	
}
