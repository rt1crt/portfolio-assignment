﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    private Rigidbody rb;
  // public Transform player;
    NavMeshAgent agent;
    public GameObject playerLayer;
    bool otherTime;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        agent = GetComponent<NavMeshAgent>();
    //    player = GameObject.Find("PlayerCharacterAni");
        playerLayer = GameObject.Find("PlayerCharacterAni");
    }

    void Update()
    {
        if(playerLayer.layer != this.gameObject.layer)
        {
            otherTime = true;
        }
        else
        otherTime = false;
    }

    void FixedUpdate()
    {
        
        agent.isStopped = otherTime;
        transform.LookAt(playerLayer.gameObject.transform);
        agent.SetDestination(playerLayer.gameObject.transform.position);


    }
}
