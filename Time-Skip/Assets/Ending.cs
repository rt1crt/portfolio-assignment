﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ending : MonoBehaviour
{
    private CapsuleCollider capCol;
    private GameObject Player;
    bool unlocked = false;
    // Start is called before the first frame update
    void Start()
    {
        capCol = GetComponent<CapsuleCollider>();
        Player = GameObject.Find("PlayerCharacterAni");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider col)
    {
        if( Player.GetComponent<PlayerController>().keysCollected >= 7)
        {
            Scene scene = SceneManager.GetActiveScene();
			SceneManager.LoadScene(scene.name);
        }
    
    }

    
}
